/*
 * decode.c
 *
 * Copyright (C) 2023 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Audio to binary converter
 *
 */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include "wav.h"

extern int verbose;

/* mp3 decoder */
#define MINIMP3_NO_SIMD
#define MINIMP3_NONSTANDARD_BUT_LOGICAL
#define MINIMP3_IMPLEMENTATION
#include "minimp3.h"

/* talking about disgusting library APIs... Things that REALLY should be in minimp3.h, but they're not... */
static void mp3dec_skip_id3v1(const uint8_t *buf, size_t *pbuf_size)
{
    size_t buf_size = *pbuf_size;
#ifndef MINIMP3_NOSKIP_ID3V1
    if (buf_size >= 128 && !memcmp(buf + buf_size - 128, "TAG", 3))
    {
        buf_size -= 128;
        if (buf_size >= 227 && !memcmp(buf + buf_size - 227, "TAG+", 4))
            buf_size -= 227;
    }
#endif
#ifndef MINIMP3_NOSKIP_APEV2
    if (buf_size > 32 && !memcmp(buf + buf_size - 32, "APETAGEX", 8))
    {
        buf_size -= 32;
        const uint8_t *tag = buf + buf_size + 8 + 4;
        uint32_t tag_size = (uint32_t)(tag[3] << 24) | (tag[2] << 16) | (tag[1] << 8) | tag[0];
        if (buf_size >= tag_size)
            buf_size -= tag_size;
    }
#endif
    *pbuf_size = buf_size;
}

static size_t mp3dec_skip_id3v2(const uint8_t *buf, size_t buf_size)
{
#define MINIMP3_ID3_DETECT_SIZE 10
#ifndef MINIMP3_NOSKIP_ID3V2
    if (buf_size >= MINIMP3_ID3_DETECT_SIZE && !memcmp(buf, "ID3", 3) && !((buf[5] & 15) || (buf[6] & 0x80) || (buf[7] & 0x80) || (buf[8] & 0x80) || (buf[9] & 0x80)))
    {
        size_t id3v2size = (((buf[6] & 0x7f) << 21) | ((buf[7] & 0x7f) << 14) | ((buf[8] & 0x7f) << 7) | (buf[9] & 0x7f)) + 10;
        if ((buf[5] & 16))
            id3v2size += 10; /* footer */
        return id3v2size;
    }
#endif
    return 0;
}

static void mp3dec_skip_id3(const uint8_t **pbuf, size_t *pbuf_size)
{
    uint8_t *buf = (uint8_t *)(*pbuf);
    size_t buf_size = *pbuf_size;
    size_t id3v2size = mp3dec_skip_id3v2(buf, buf_size);
    if (id3v2size)
    {
        if (id3v2size >= buf_size)
            id3v2size = buf_size;
        buf      += id3v2size;
        buf_size -= id3v2size;
    }
    mp3dec_skip_id3v1(buf, &buf_size);
    *pbuf = (const uint8_t *)buf;
    *pbuf_size = buf_size;
}

static int mp3dec_check_vbrtag(const uint8_t *frame, int frame_size, uint32_t *frames, int *delay, int *padding)
{
    static const char g_xing_tag[4] = { 'X', 'i', 'n', 'g' };
    static const char g_info_tag[4] = { 'I', 'n', 'f', 'o' };
#define FRAMES_FLAG     1
#define BYTES_FLAG      2
#define TOC_FLAG        4
#define VBR_SCALE_FLAG  8
    /* Side info offsets after header:
    /                Mono  Stereo
    /  MPEG1          17     32
    /  MPEG2 & 2.5     9     17*/
    bs_t bs[1];
    L3_gr_info_t gr_info[4];
    bs_init(bs, frame + HDR_SIZE, frame_size - HDR_SIZE);
    if (HDR_IS_CRC(frame))
        get_bits(bs, 16);
    if (L3_read_side_info(bs, gr_info, frame) < 0)
        return 0; /* side info corrupted */

    const uint8_t *tag = frame + HDR_SIZE + bs->pos/8;
    if (memcmp(g_xing_tag, tag, 4) && memcmp(g_info_tag, tag, 4))
        return 0;
    int flags = tag[7];
    if (!((flags & FRAMES_FLAG)))
        return -1;
    tag += 8;
    *frames = (uint32_t)(tag[0] << 24) | (tag[1] << 16) | (tag[2] << 8) | tag[3];
    tag += 4;
    if (flags & BYTES_FLAG)
        tag += 4;
    if (flags & TOC_FLAG)
        tag += 100;
    if (flags & VBR_SCALE_FLAG)
        tag += 4;
    *delay = *padding = 0;
    if (*tag)
    {   /* extension, LAME, Lavc, etc. Should be the same structure. */
        tag += 21;
        if (tag - frame + 14 >= frame_size)
            return 0;
        *delay   = ((tag[0] << 4) | (tag[1] >> 4)) + (528 + 1);
        *padding = (((tag[1] & 0xF) << 8) | tag[2]) - (528 + 1);
    }
    return 1;
}

typedef struct
{
    int16_t *buffer;
    size_t samples; /* channels included, byte size = samples*sizeof(mp3d_sample_t) */
    int channels, hz, layer, avg_bitrate_kbps;
} mp3dec_file_info_t;

#define MP3D_E_MEMORY   -1
#define MP3D_E_PARAM    -2
#define MP3D_E_DECODE   -3
int mp3dec_load_buf(mp3dec_t *dec, uint8_t *buf, size_t buf_size, mp3dec_file_info_t *info)
{
    if (!dec || !buf || !info || (size_t)-1 == buf_size)
        return MP3D_E_PARAM;
    uint64_t detected_samples = 0;
    size_t orig_buf_size = buf_size;
    int to_skip = 0;
    mp3dec_frame_info_t frame_info;
    memset(info, 0, sizeof(*info));
    memset(&frame_info, 0, sizeof(frame_info));

    /* skip id3 */
    size_t filled = 0, consumed = 0;
    int eof = 0, ret = 0;
    mp3dec_skip_id3((const uint8_t **)&buf, &buf_size);
    if (!buf_size)
        return 0;
    /* try to make allocation size assumption by first frame or vbr tag */
    mp3dec_init(dec);
    int samples;
    do
    {
        uint32_t frames;
        int i, delay, padding, free_format_bytes = 0, frame_size = 0;
        const uint8_t *hdr;
        i = mp3d_find_frame(buf, buf_size, &free_format_bytes, &frame_size);
        buf      += i;
        buf_size -= i;
        hdr = buf;
        if (i && !frame_size)
            continue;
        if (!frame_size)
            return 0;
        frame_info.channels = HDR_IS_MONO(hdr) ? 1 : 2;
        frame_info.hz = hdr_sample_rate_hz(hdr);
        frame_info.layer = 4 - HDR_GET_LAYER(hdr);
        frame_info.bitrate_kbps = hdr_bitrate_kbps(hdr);
        frame_info.frame_bytes = frame_size;
        samples = hdr_frame_samples(hdr)*frame_info.channels;
        if (3 != frame_info.layer)
            break;
        int ret = mp3dec_check_vbrtag(hdr, frame_size, &frames, &delay, &padding);
        if (ret > 0)
        {
            padding *= frame_info.channels;
            to_skip = delay*frame_info.channels;
            detected_samples = samples*(uint64_t)frames;
            if (detected_samples >= (uint64_t)to_skip)
                detected_samples -= to_skip;
            if (padding > 0 && detected_samples >= (uint64_t)padding)
                detected_samples -= padding;
            if (!detected_samples)
                return 0;
        }
        if (ret)
        {
            buf      += frame_size;
            buf_size -= frame_size;
        }
        break;
    } while(1);
    size_t allocated = MINIMP3_MAX_SAMPLES_PER_FRAME*sizeof(mp3d_sample_t);
    if (detected_samples)
        allocated += detected_samples*sizeof(mp3d_sample_t);
    else
        allocated += (buf_size/frame_info.frame_bytes)*samples*sizeof(mp3d_sample_t);
    info->buffer = (mp3d_sample_t*)malloc(allocated);
    if (!info->buffer)
        return MP3D_E_MEMORY;
    /* save info */
    info->channels = frame_info.channels;
    info->hz       = frame_info.hz;
    info->layer    = frame_info.layer;
    /* decode all frames */
    size_t avg_bitrate_kbps = 0, frames = 0;
    do
    {
        if ((allocated - info->samples*sizeof(mp3d_sample_t)) < MINIMP3_MAX_SAMPLES_PER_FRAME*sizeof(mp3d_sample_t))
        {
            allocated *= 2;
            mp3d_sample_t *alloc_buf = (mp3d_sample_t*)realloc(info->buffer, allocated);
            if (!alloc_buf)
                return MP3D_E_MEMORY;
            info->buffer = alloc_buf;
        }
        samples = mp3dec_decode_frame(dec, buf, MINIMP3_MIN(buf_size, (size_t)INT_MAX), info->buffer + info->samples, &frame_info);
        buf      += frame_info.frame_bytes;
        buf_size -= frame_info.frame_bytes;
        if (samples)
        {
            if (info->hz != frame_info.hz || info->layer != frame_info.layer)
            {
                ret = MP3D_E_DECODE;
                break;
            }
            if (info->channels && info->channels != frame_info.channels)
            {
                ret = MP3D_E_DECODE;
                break;
            }
            samples *= frame_info.channels;
            if (to_skip)
            {
                size_t skip = MINIMP3_MIN(samples, to_skip);
                to_skip -= skip;
                samples -= skip;
                memmove(info->buffer, info->buffer + skip, samples*sizeof(mp3d_sample_t));
            }
            info->samples += samples;
            avg_bitrate_kbps += frame_info.bitrate_kbps;
            frames++;
        }
    } while (frame_info.frame_bytes);
    if (detected_samples && info->samples > detected_samples)
        info->samples = detected_samples; /* cut padding */
    /* reallocate to normal buffer size */
    if (allocated != info->samples*sizeof(mp3d_sample_t) && info->samples > 1)
    {
        mp3d_sample_t *alloc_buf = (mp3d_sample_t*)realloc(info->buffer, info->samples*sizeof(mp3d_sample_t));
        if (!alloc_buf && info->samples)
            return MP3D_E_MEMORY;
        info->buffer = alloc_buf;
    }
    if (frames)
        info->avg_bitrate_kbps = avg_bitrate_kbps/frames;
    return ret;
}

mp3dec_file_info_t info;

/**
 * Verify that all sync bits are 1s. Returns start offset or -1 on error.
 */
int syncmatch(int sy, int length, int nb, int ng, int hdr, int skip)
{
    int i, j, k, l, m, n, o, p, q;

    for(l = skip > 0 ? skip : length / 2; l < info.samples && info.buffer[l] < 0; l++);
    if(sy == 2) { l -= length; if(l < skip - length) l = skip - length; if(l < 0) l = 0; }
    m = sy == 3 ? 3 * length : 2 * length;
    if(verbose) printf("looking for the most likely start offset (from %d, step %d)\n", l, m);
    for(q = l; l < info.samples / 2; l++) {
        /* we check 1024 sync bits at most */
        k = 1024; n = l + k * m; if(n > info.samples - m) { n = info.samples - m; k = (n - l) / m; }
        for(i = o = l, j = p = 0; i < n && j < k;) {
            if(sy & 1) {
                if(info.buffer[i] < 0 && info.buffer[i + 1] > 0) i++; else if(info.buffer[i] < 0) break;
                while(i > o && info.buffer[i - 1] > 0) { i--; } i += length;
            }
            i += length;
            if(sy & 2) {
                if(info.buffer[i] < 0 && info.buffer[i + 1] > 0) i++; else if(info.buffer[i] < 0) break;
                while(i > o && info.buffer[i - 1] > 0) { i--; } i += length;
            }
            p++; if(p >= nb) { p = 0; i += ng; }
            o = i; j++;
        }
        if(j == k) {
            /* Hack: detect some funky leading emptyness on certain Homelab tapes. q = first bit set, l = start of data */
            if(hdr && sy == 2 && l - (q >= l ? 0 : q) - skip >= 16 * length) {
                /* if the sample looks like this...
                 * _XXX_XXXXXXX_XX_________________________________________________________??????___XXX___XXX___XXX___XXX___XXX_...
                 *  ^q (some bits)  ^j (at least 7 bits worth of total emptiness, no sync bits)  ^l  (normal data with sync bits) */
                j = l - 14 * length; if(q > j) q = j;
                for(m = n = 0; q < l; q++) if(info.buffer[q] < 0) { n++; if(n > m) m = n; } else n = 0;
                /* ...then add an extra 7 zero bits at the begining, because first data bit will be the first byte's 8th bit
                 * (note, we're using 14 here not 7 because we have 2*length per bit) */
                if(m >= 14 * length) {
                    if(verbose) printf("found header: 7 bits worth syncless emptyness between first set bit and actual data\n");
                    l = j;
                }
            }
            return l;
        }
    }
    return -1;
}

/**
 * Decode audio into binary
 */
uint8_t *decode(uint8_t *data, size_t size, int fmt, int en, int nb, int sy, int fm, int ng, int length, int hdr, int skip, size_t *outlen)
{
    int ret, i, j, k, l, m, n, o = 0, q, len, dist[1024];
    mp3dec_t mp3d;
    uint8_t *ptr, *end, *out;
    wav_header_t *wav = (wav_header_t*)data;

    *outlen = 0;
    if(ng < 0) ng = 0;
    if(ng > 4096) ng = 4096;

    memset(&info, 0, sizeof(info));
    /* is it an mp3 file? */
    if((ret = mp3dec_load_buf(&mp3d, data, size, &info)) < 0 || info.channels < 1 || info.samples < 1 || !info.buffer) {
        if(info.buffer) { free(info.buffer); info.buffer = NULL; }
        /* is it a wav file? */
        if(size > 44 && !memcmp(data, "RIFF", 4) && !memcmp(data + 8, "WAVEfmt", 7) && (data[20] == 1 || data[20] == 3) &&
          (data[22] == 1 || data[22] == 2) && !data[23] && data[32] && !data[33] && !data[35]) {
            end = data + size;
            ptr = data + 12; while(memcmp(ptr, "data", 4) && ptr < end) { ptr += ((ptr[6]<<16)|(ptr[5]<<8)|ptr[4]) + 8; }
            if(ptr < end && !memcmp(ptr, "data", 4)) {
                ptr += 4; l = (ptr[3]<<24)|(ptr[2]<<16)|(ptr[1]<<8)|ptr[0]; ptr += 4; if(end > ptr + l) end = ptr + l;
                info.samples = l / wav->frame_size; info.hz = wav->sample_rate;
                info.buffer = (int16_t*)malloc(info.samples * sizeof(int16_t));
                if(!info.buffer) return NULL;
                if(verbose)
                    printf("wav %d channels, %d samples, %d bits (%s)\n", wav->channels, (int)info.samples, wav->bit_depth,
                        wav->audio_format == 1 ? "integer" : "float");
                for(i = 0; i < info.samples && ptr < end; i++) {
                    for(ret = j = 0; j < wav->channels && ptr < end; j++)
                        switch(wav->bit_depth) {
                            case 8: ret += (*((int8_t*)ptr++) - (fmt ? 0 : -128)) << 8; break;
                            case 16: ret += *((int16_t*)ptr); ptr += 2; break;
                            case 24: ptr++; ret += *((int16_t*)ptr); ptr += 2; break;
                            case 32:
                                if(wav->audio_format == 1) { ptr += 2; ret += *((int16_t*)ptr); ptr += 2; }
                                else { ret += (int)((float)((ptr[3]<<24)|(ptr[2]<<16)|(ptr[1]<<8)|ptr[0]) * 32767.0f); ptr += 4; }
                            break;
                        }
                    info.buffer[i] = ret / wav->channels;
                }
            }
        }
    } else {
        if(verbose) printf("mp3 %d channels, %d samples, 16 bits (integer)\n", info.channels, (int)info.samples);
        /* convert to mono */
        if(info.channels != 1) {
            for(i = j = 0; i < info.samples; i += info.channels) {
                for(ret = l = 0; l < info.channels; l++) ret += info.buffer[i + l];
                info.buffer[j++] = ret / info.channels;
            }
            info.samples = j;
        }
    }
    if(info.samples < 1 || !info.buffer || skip >= info.samples) return NULL;

    /* trim silence at the end */
    for(i = 0; info.samples > info.hz / 16 && info.buffer[info.samples - 1] < 16 && info.buffer[info.samples - 1] > -16; info.samples--) i++;
    if(i > info.hz / 16) info.samples += info.hz / 16;

    /* find min and max values */
    for(i = skip, j = 32767, l = -32768; i < info.samples; i++) {
        if(info.buffer[i] < j) j = info.buffer[i];
        if(info.buffer[i] > l) l = info.buffer[i];
    }
    /* reduce noise, normalize signal */
    for(i = skip; i < info.samples; i++) {
        m = info.buffer[i] < j ? j - info.buffer[i] : info.buffer[i] - j;
        n = info.buffer[i] < l ? l - info.buffer[i] : info.buffer[i] - l;
        if(m < n) info.buffer[i] = -32768; else info.buffer[i] = 32767;
    }

    if(verbose)
        printf("%s %d samples, min %d, max %d, freq %d, length %d usec, gap %d usec\n", fm ? "FM" : "AM", (int)info.samples - skip,
            j, l, info.hz, length, ng);
    length = (uint64_t)length * (uint64_t)info.hz / 1000000;
    ng = (uint64_t)ng * (uint64_t)info.hz / 1000000;

    if(fm) {
        /* find the shortest and longest distance for frequency modulation */
        memset(dist, 0, sizeof(dist));
        for(i = skip + 1, l = n = 0; i < info.samples; i++) {
            if(((fm & 1) && info.buffer[i] < 0 && info.buffer[i - 1] > 0) ||
               ((fm & 2) && info.buffer[i] > 0 && info.buffer[i - 1] < 0)) {
                if(i - l > 1 && i - l < (int)(sizeof(dist)/sizeof(dist[0]))) {
                    dist[i - l]++; if(dist[i - l] > n) n = dist[i - l];
                }
                l = i;
            }
        }
        for(i = 0, j = l = -1; i < (int)(sizeof(dist)/sizeof(dist[0])); i++)
            if(dist[i] < n / 8) dist[i] = 0; else
            if(dist[i]) {
                if(j == -1) j = i;
                l = i;
            }
        k = (l - j) / 2 + j;
        if(verbose) {
            printf("short %d, long %d avg %d\n", j, l, k);
            for(i = 0; i < (int)(sizeof(dist)/sizeof(dist[0])); i++)
                if(dist[i]) printf("distance %3d occourance %8d\n", i, dist[i]);
        }
        if(length < 4 || length > 4096 || length >= info.samples) length = j;
        if(ng < 1) ng = l - j;
        len = (info.samples - skip) / nb / length;
        if(verbose)
            printf("max number of bits in audio %d, 1 bit %d - %d samples, %d %s endian bits per byte (max %d bytes)\n",
                (int)((info.samples - skip) / length), length, length + ng, nb, en ? "little" : "big", len);
        /* place bits in output buffer */
        out = (uint8_t*)malloc(len + 4096);
        if(!out) { free(info.buffer); return NULL; }
        memset(out, 0, len + 4096);
        for(i = skip + 1, j = l = 0; i < info.samples; i++) {
            if(((fm & 1) && info.buffer[i] < 0 && info.buffer[i - 1] > 0) ||
               ((fm & 2) && info.buffer[i] > 0 && info.buffer[i - 1] < 0)) {
                if(verbose > 1) printf("%s pos %8d (%3d) byte %8d bit %d\n", i && !(j % nb) ? "--------\n" : "", (int)i, i - l, j / nb, i - l > k);
                if(i - l > k && (j % nb) < 8) out[j / nb] |= 1 << (en ? j % nb : (nb - 1) - (j % nb));
                l = i; j++;
            }
        }
        len = j / nb;
    } else {
        /* determine the smallest distance between to up-edges */
        memset(dist, 0, sizeof(dist));
        for(i = skip, l = n = 0; i < info.samples; i++) {
            if(info.buffer[i] > 0 && (i == skip || info.buffer[i - 1] < 0)) {
                j = i - l; l = i;
                if(j > 1 && j < (int)(sizeof(dist)/sizeof(dist[0]))) {
                    dist[j]++; if(dist[j] > n) n = dist[j];
                }
            }
        }
        for(i = l = m = 0; i < (int)(sizeof(dist)/sizeof(dist[0])); i++)
            if(dist[i] < n / 8) dist[i] = 0; else
            if(dist[i] > l) { l = dist[i]; m = i; }
        if(length < 1 || length > 4096 || length >= info.samples) {
            if(sy) {
                if(sy == 3) length = m / 3; else length = m / 2;
            } else
                for(length = 0; length < (int)(sizeof(dist)/sizeof(dist[0])) && !dist[length]; length++);
            if(verbose) {
                for(i = 0; i < (int)(sizeof(dist)/sizeof(dist[0])); i++)
                    if(dist[i]) printf("distance %3d occourance %8d%s\n", i, dist[i], i == length ? " (length)" : "");
                printf("most likely bit length %d samples\n", length);
            }
        }
        if(length < 4 || length > 4096 || length >= info.samples) length = 4;
        /* find start offset by counting how many sync bits match */
        if(sy) {
            o = syncmatch(sy, length, nb, ng, hdr, skip);
            if(o < 0) {
                for(i = l = length = 0; i < (int)(sizeof(dist)/sizeof(dist[0])); i++)
                    if(i != m && dist[i] > l) { l = dist[i]; length = i; }
                if(sy == 3) length /= 3; else length /= 2;
                if(verbose) printf("sync bits don't match, trying 2nd most occoured length %d\n", length);
                o = syncmatch(sy, length, nb, ng, hdr, skip);
                if(o < 0) {
                    /* try some other lengths as well */
                    for(i = 1; i < length - 1; i++) {
                        if((o = syncmatch(sy, length - i, nb, ng, hdr, skip)) >= 0) { length -= i; break; }
                        if((o = syncmatch(sy, length + i, nb, ng, hdr, skip)) >= 0) { length += i; break; }
                    }
                }
                if(o < 0) {
                    if(verbose) printf("unable to find a valid length where all sync bits are set\n");
                    free(info.buffer); return NULL;
                }
            }
            if(verbose) printf("start offset %d\n", o);
        }
        j = (sy == 3 ? 3 : (sy ? 2 : 1)); l = nb * j;
        len = (info.samples - skip) / nb / length / j + 1;
        if(verbose)
            printf("number of bits in audio %d, 1 bit %d samples (%d with %d%d sync), %d %s endian bits per byte, gap %d samples (max %d bytes)\n",
                (int)((info.samples - skip) / length), length, j * length, sy & 1, sy & 2 ? 1 : 0, nb, en ? "little" : "big", ng, len);
        /* place bits in output buffer, we allocate 4k more in case sync bits are shifting widely */
        out = (uint8_t*)malloc((info.samples - skip) / nb / length + 4096);
        if(!out) { free(info.buffer); return NULL; }
        memset(out, 0, (info.samples - skip) / nb / length + 4096);
        q = length / 2 - 1; if(q < 1) q = 1;
        for(i = o, j = 0; i < info.samples; j++) {
            if(sy & 1) {
                /* look for leading sync bit, and seek to its beginning */
                if(info.buffer[i] < 0 && info.buffer[i + 1] > 0) i++;
                while(i > o && info.buffer[i - 1] > 0) { i--; } i += length;
            }
            /* examine the close surroundings of current seek poisition */
            for(k = -q; k < q && info.buffer[i + k] < 0; k++);
            if(verbose > 1)
                printf("%s pos %8d byte %8d bit %d\n", i && !(j % nb) ? "--------\n" : "", (int)i + (k < q ? k : 0), j / nb, k < q);
            if(k < q && (j % nb) < 8) {
                out[j / nb] |= 1 << (en ? j % nb : (nb - 1) - (j % nb));
                /* in lack of sync bits, if we have a one bit, seek to its beginning to counteract rounding errors */
                if(!sy) for(i += k; info.buffer[i - 1] > 0; i--);
            }
            /* jump over data bit */
            i += length;
            if(sy & 2) {
                /* look for trailing sync bit, and seek to its beginning */
                if(info.buffer[i] < 0 && info.buffer[i + 1] > 0) i++;
                while(i > o && info.buffer[i - 1] > 0) { i--; } i += length;
            }
            if(j && (j / nb) != ((j - 1) / nb)) i += ng;
            o = i;
        }
        len = j / nb;
    }
    if(verbose) printf("binary returned %d bytes\n", len);
    free(info.buffer);
    *outlen = len;
    return out;
}

/**
 * WASM helper
 */
int dodecode(uint8_t *data, size_t size, int fmt, int en, int nb, int sy, int fm, int ng, int length, int hdr, int skip, uint8_t *out, int outmax)
{
    size_t outlen = 0;
    uint8_t *buf;
    verbose = 1;
    printf("-------- decode --------\n");
    buf = decode(data, size, fmt, en, nb, sy, fm, ng, length, !hdr, skip, &outlen);
    if(outlen > (size_t)outmax) outlen = outmax;
    if(buf && outlen > 0) { memcpy(out, buf, outlen); free(buf); }
    return outlen;
}
