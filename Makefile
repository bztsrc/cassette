TARGET=cassette

SRCS=$(wildcard *.c) $(wildcard libmp3lame/*.c)
OBJS=$(SRCS:.c=.o)
CFLAGS=-I. -Ilibmp3lame -DHAVE_CONFIG_H -g
LIBS=-lm

all: $(TARGET)

wasm: $(SRCS)
	emcc -s WASM=1 -s ALLOW_MEMORY_GROWTH=1 -s EXPORTED_FUNCTIONS='["_dodecode","_doencode","_malloc","_free"]' -s EXPORTED_RUNTIME_METHODS='["ccall","cwrap"]' $(CFLAGS) $(SRCS) -o public/$(TARGET).js

%.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@

$(TARGET): $(OBJS)
	$(CC) $(OBJS) -o $(TARGET) $(LIBS)

clean:
	rm $(TARGET) $(TARGET).exe public/$(TARGET).js public/$(TARGET).wasm *.o libmp3lame/*.o 2>/dev/null || true
