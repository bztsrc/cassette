/*
 * wav.h
 *
 * Copyright (C) 2023 bzt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * @brief Wav format
 *
 */

/* riff wave header */
typedef struct {
    char str_riff[4];   /* "RIFF" */
    int wav_size;       /* (file size) - 8 */
    char str_wave[4];   /* "WAVE" */

    /* Format Header */
    char str_fmt[4];    /* "fmt " */
    int fmt_chunk_size; /* Should be 16 for PCM */
    short audio_format; /* Should be 1 for PCM. 3 for IEEE Float */
    short channels;     /* number of channels */
    int sample_rate;    /* eg: 8000, 44100, 48000 */
    int byte_rate;      /* Number of bytes per second. sample_rate * channels * Bytes Per Sample */
    short frame_size;   /* channels * Bytes Per Sample */
    short bit_depth;    /* bits per sample, eg: 8, 16, 24 */

    /* Data chunk */
    char str_data[4];   /* "data" */
    int data_bytes;     /* (file size) - 44 */
} wav_header_t;
